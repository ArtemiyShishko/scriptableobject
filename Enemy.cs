﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _hp;

    public void TakeDamage(float damage)
    {
        _hp -= damage;
    }
}
