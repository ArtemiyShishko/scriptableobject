﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _hp;
    [SerializeField] private string _name;
    [SerializeField] private Weapon _weapon;

    public Weapon weapon
    {
        get
        {
            return _weapon;
        }
        set
        {
            _weapon = weapon;
        }
    }
}
