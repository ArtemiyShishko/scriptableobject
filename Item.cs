﻿using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField] private int _id;
    [SerializeField] private string _name;
    [SerializeField]private int _price;
    [SerializeField] private GameObject prefab;

    public string Name => _name;
    public int Price => _price;
    public int Id => _id;

    private void OnValidate()
    {
        if (_price < 0)
            _price = 0;
    }
}

[CreateAssetMenu(menuName = "Items/Weapon")]
public class Weapon : Item
{
    [SerializeField] private float _damage;
    public float Damage => _damage;

    public float DealDamage() 
    { 
        return _damage;
    }
}
[CreateAssetMenu(menuName = "Items/Food")]
public class Food : Item
{
    [SerializeField] private int _restoreValue;
    public int RestoreValue => _restoreValue;

    public int Eat()
    {
        return _restoreValue;
    }

}